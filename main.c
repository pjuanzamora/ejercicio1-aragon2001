#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*Ejercicio 1 - Arag�n 2001 Ficheros binarios y busqueda binaria con registros*/

//Estructura
typedef struct{
	char nombre[50];
	int num_mat;
	char tipo_r;
}Registro;

void escribirRegistros();
void leerFichero(Registro vReg[100]);



int main(int argc, char *argv[]) {
	int i;
	//escribirRegistros(); //Grabo dos registros para realizar pruebas
	Registro vRegistros[1000];
	
	leerFichero(vRegistros); //Lectura del fichero
	for (i = 0; i<2; i++){
		printf("Nombre %s - Matricula %i \n", vRegistros[i].nombre, vRegistros[i].num_mat);
	}
	
	return 0;
}


void escribirRegistros(){
	FILE *f = fopen("fich1.dat", "wb+");
	Registro r,r1;
	int aux;
	
	strcpy(r.nombre,"Juan");
	r.num_mat = 1;
	r.tipo_r = 'a';
	strcpy(r1.nombre,"Pepe");
	r1.num_mat = 2;
	r1.tipo_r = 'b';
	
	if (f==NULL){
		printf("Error al abrir el fichero");
	}
	aux = fwrite(&r,sizeof(Registro), 1, f);
	aux = fwrite(&r1,sizeof(Registro), 1, f);
	fclose(f);
}

void leerFichero(Registro vReg[100]){
	FILE *f = fopen("fich1.dat", "rb");
	int i;
	
	if (f==NULL){
		printf("Fichero no encontrado");
		exit(0);
	}
	
	for (i=0; !feof(f); i++){
		fread(&vReg[i], sizeof(Registro), 1, f);
	}
	fclose(f);
}
